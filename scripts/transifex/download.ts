import {
  transifexGetRaw,
  transifexGetRawString,
  transifexPost,
  TransifexRedirect,
} from "./api";

export async function resourceTranslationDownload(id: string): Promise<string> {
  const data = await transifexPost("/resource_translations_async_downloads", {
    data: {
      attributes: {
        content_encoding: "text",
      },
      relationships: {
        language: { data: { id: "l:it", type: "languages" } },
        resource: { data: { id, type: "resources" } },
      },
      type: "resource_translations_async_downloads",
    },
  });

  const jobUrl = data.data.links.self;
  while (true) {
    let result: any;
    try {
      result = await transifexGetRaw(jobUrl);
    } catch (e) {
      if (e instanceof TransifexRedirect) {
        return await transifexGetRawString(e.url);
      }
      throw e;
    }

    const status = result?.data?.attributes?.status;
    if (status === "failed") {
      throw new Error(
        result?.data?.attributes?.errors
          ?.map((e: any) => `${e.code}: ${e.detail}`)
          .join(", ") ?? "generic error"
      );
    }
    if (status !== "pending" && status !== "processing") {
      throw new Error("unexpected status");
    }
    await delay(200);
  }
}

export function delay(ms: number) {
  return new Promise<void>((ful) => setTimeout(() => ful(), ms));
}
