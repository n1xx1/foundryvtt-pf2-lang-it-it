import fetch, { isRedirect } from "node-fetch";

const TRANSIFEX_TOKEN = process.env.TTOKEN ?? "?";
const TRANSIFEX_URL = "https://rest.api.transifex.com";

export async function transifexGetAll(url: string, query: any): Promise<any[]> {
  const queryString = new URLSearchParams(query).toString();
  if (queryString.length > 0) url += `?${queryString}`;
  url = `${TRANSIFEX_URL}${url}`;

  const actualData = [];
  while (true) {
    const data = await transifexGetRaw(url);
    actualData.push(...data.data);

    if (data.links.next) {
      url = data.links.next;
      continue;
    }
    break;
  }
  return actualData;
}

export async function transifexGet(url: string, query: any): Promise<any> {
  const queryString = new URLSearchParams(query).toString();
  if (queryString.length > 0) url += `?${queryString}`;
  return await transifexGetRaw(`${TRANSIFEX_URL}${url}`);
}

export class TransifexRedirect extends Error {
  constructor(public url: string, public status: number) {
    super("redirect");
  }
}

export async function transifexGetRaw(url: string): Promise<any> {
  const resp = await fetch(url, {
    headers: {
      Authorization: `Bearer ${TRANSIFEX_TOKEN}`,
    },
    redirect: "manual",
  });
  if (isRedirect(resp.status)) {
    throw new TransifexRedirect(resp.headers.get("location"), resp.status);
  }
  return await resp.json();
}

export async function transifexGetRawString(url: string): Promise<string> {
  const resp = await fetch(url, {
    headers: {
      Authorization: `Bearer ${TRANSIFEX_TOKEN}`,
    },
    redirect: "manual",
  });
  if (isRedirect(resp.status)) {
    throw new TransifexRedirect(resp.headers.get("location"), resp.status);
  }
  return await resp.text();
}

export async function transifexPost(url: string, body: any): Promise<any> {
  const resp = await fetch(`${TRANSIFEX_URL}${url}`, {
    headers: {
      Authorization: `Bearer ${TRANSIFEX_TOKEN}`,
      "Content-Type": "application/vnd.api+json",
    },
    method: "post",
    body: JSON.stringify(body),
  });
  if (!resp.ok) {
    throw new Error(`${resp.status} - ${await resp.text()}`);
  }
  if (isRedirect(resp.status)) {
    throw new TransifexRedirect(resp.headers.get("location"), resp.status);
  }
  return await resp.json();
}
