import { mkdir, readdir, unlink, writeFile } from "fs/promises";
import { join } from "path";
import { resourceTranslationDownload } from "./transifex/download";

async function setupOut() {
  const dir = "out/compendium";
  await mkdir(dir, { recursive: true }).catch((e) => Promise.resolve());
  for (const file of await readdir(dir)) {
    await unlink(join(dir, file));
  }
  await unlink("out/it.json").catch((e) => Promise.resolve());
}

async function transifexDownload(resource: string, dest: string) {
  console.log(`downloading resource ${resource} from transifex`);
  let data = await resourceTranslationDownload(
    `o:foundryvtt-ita:p:pathfinder-2e-2:r:${resource}`
  );

  try {
    JSON.parse(data);
  } catch (e) {
    console.warn(`attempting to fix transifex bug for ${dest}`);
    data = fixTransifexBug(data);
    try {
      JSON.parse(data);
    } catch (e) {
      console.error(`failed to validate json: ${dest}`);
      process.exit(-1);
    }
  }
  await writeFile(dest, data);
}

async function main() {
  await setupOut();

  for (const [compendium, resource] of Object.entries(resources)) {
    if (!resource) continue;
    await transifexDownload(resource, `out/compendium/pf2e.${compendium}.json`);
  }
  await transifexDownload("enjson", `out/it.json`);
}

function fixTransifexBug(x: string) {
  return x.replace(/(..)"(..)/g, (m, b, a) => {
    if (b[1] == "\\") return m;
    if (b == "  ") return m;
    if (b == ": " || a == ": ") return m;
    if (a == ",\n") return m;
    return `${b}\\"${a}`;
  });
}

const resources = {
  actionspf2e: "actions",
  "age-of-ashes-bestiary": "age-of-ashes-bestiary",
  ancestries: "ancestries",
  ancestryfeatures: "ancestryfeatures",
  archetypes: "archetypes",
  backgrounds: "pf2ebackgroundsjson",
  "pathfinder-bestiary": "bestiary-1",
  classes: "classes",
  classfeatures: "classfeatures",
  conditionitems: "conditionitems",
  deities: "deities",
  "equipment-srd": "equipment-srd",
  "fall-of-plaguestone-bestiary": "fall-of-plaguestone-bestiary",
  "feats-srd": "feats",
  heritages: "heritages",
  "npc-gallery": "npc-gallery",
  "spells-srd": "spells",
};

main();
