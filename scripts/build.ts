import { build } from "esbuild";
import { readFile, writeFile } from "fs/promises";
import { join } from "path";
import { FoundryModuleManifest } from "./foundry/types";

const envBuild = process.env.CI_PIPELINE_IID ?? "000";
const envBranch = process.env.CI_COMMIT_BRANCH ?? "test";
const envRepo = process.env.CI_PROJECT_PATH ?? "example";

const trimPrefix = (s: string, prefix: string) =>
  s.startsWith(prefix) ? s.substring(prefix.length) : s;

build({
  entryPoints: ["src/module.js"],
  outdir: "out",
  bundle: true,
  format: "cjs",
  metafile: true,
  plugins: [
    {
      name: "copy manifest",
      setup(build) {
        build.onEnd(async (result) => {
          const manifest: FoundryModuleManifest = JSON.parse(
            await readFile("./src/module.json", "utf-8")
          );

          manifest.esmodules = Object.keys(result.metafile.outputs)
            .filter((x) => x.endsWith(".js"))
            .map((x) => trimPrefix(x, build.initialOptions.outdir + "/"));

          manifest.styles = Object.keys(result.metafile.outputs)
            .filter((x) => x.endsWith(".css"))
            .map((x) => trimPrefix(x, build.initialOptions.outdir + "/"));

          manifest.version = `${manifest.version}.${envBuild}`;
          manifest.url = `https://gitlab.com/${envRepo}`;
          manifest.manifest = `https://gitlab.com/${envRepo}/-/jobs/artifacts/${envBranch}/raw/module.json?job=build`;
          manifest.download = `https://gitlab.com/${envRepo}/-/jobs/artifacts/${envBranch}/raw/module.zip?job=build`;

          await writeFile(
            join(build.initialOptions.outdir, "module.json"),
            JSON.stringify(manifest, null, 2)
          );
        });
      },
    },
  ],
});
