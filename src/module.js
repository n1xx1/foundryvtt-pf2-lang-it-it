import "./style.css";

const MODULE_NAME = "FoundryVTT-pf2-it";

const TRANSLATE_MONEY = {
  pp: "mp",
  gp: "mo",
  sp: "ma",
  cp: "mr",
};

const TRANSLATE_RANGE = {
  touch: "Contatto",
  planetary: "Planetario",
  unlimited: "Illimitato",
  "see text": "Vedi testo",
  varies: "Vario",
};

function convertEnabled() {
  return game.settings.get(MODULE_NAME, "convert");
}

const allowedTimes = ["1", "2", "3", "free", "reaction"];

function round(num) {
  return Math.round((num + Number.EPSILON) * 100) / 100;
}

function convertFeet(v) {
  if (v % 5 == 0) {
    return (v / 5) * 1.5;
  }
  return round(v * 0.3);
}

function convertMiles(v) {
  return round(v * 1.6);
}

function convertFeetString(v) {
  return convertFeet(parseInt(v.replace(/,/g, ""))).toLocaleString("it-IT");
}

function convertMilesString(v) {
  return convertMiles(parseInt(v.replace(/,/g, ""))).toLocaleString("it-IT");
}

Hooks.once("init", () => {
  if (typeof Babele === "undefined") return;

  game.settings.register(MODULE_NAME, "convert", {
    name: "Conversione Automatica",
    hint: "Abilita conversione automatica delle unità nel sistema metrico.",
    scope: "world",
    type: Boolean,
    default: true,
    config: true,
  });

  const babele = Babele.get();

  babele.register({
    module: MODULE_NAME,
    lang: "it",
    dir: "compendium",
  });

  const translateMoneyRegex = new RegExp(
    Object.keys(TRANSLATE_MONEY).join("|"),
    "gi"
  );

  babele.registerConverters({
    pfitCoins(value) {
      if (!value || !convertEnabled()) return value;

      return value
        .toString()
        .replace(translateMoneyRegex, (m) => TRANSLATE_MONEY[m]);
    },
    pfitRange(range, translations) {
      if (!range || !convertEnabled()) return range;

      range = range.toLowerCase();
      let m;
      if (TRANSLATE_RANGE[range]) {
        return TRANSLATE_RANGE[range];
      }
      if ((m = range.match(/^([\d.,]+)(| feet| miles?)$/))) {
        if (m[2] === " feet" || m[2] === "") {
          return `${convertFeetString(m[1])} metri`;
        }
        if (m[2].startsWith(" mile")) {
          return `${convertMilesString(m[1])} miglia`;
        }
      }
      return translations ?? range;
    },
    pfitTime(time, translations) {
      if (!time || !convertEnabled()) return time;

      time = time.toLowerCase();
      let m;
      if (allowedTimes.includes(time)) {
        return time;
      }
      if ((m = time.match(/^([\d.,]+) (minutes?|days?|hours?)$/))) {
        if (m[2].startsWith("minute")) {
          return `${m[1]} minut${m[2].endsWith("s") ? "i" : "o"}`;
        }
        if (m[2].startsWith("hour")) {
          return `${m[1]} or${m[2].endsWith("s") ? "e" : "a"}`;
        }
        if (m[2].startsWith("day")) {
          return `${m[1]} giorn${m[2].endsWith("s") ? "i" : "o"}`;
        }
      }
      return translations ?? time;
    },
    pfitLength: (feet) => {
      if (feet && convertEnabled()) {
        feet = convertFeet(feet);
      }
      return feet;
    },
    pfitSpeeds: (speed) => {
      if (speed && convertEnabled()) {
        if (speed.value) {
          speed.value =
            typeof speed.value === "stirng"
              ? convertFeetString(speed.value)
              : convertFeet(speed.value);
        }
        if (speed.otherSpeeds && Array.isArray(speed.otherSpeeds)) {
          speed.otherSpeeds = speed.otherSpeeds.map((s) =>
            s.value
              ? {
                  ...s,
                  value:
                    typeof s.value === "stirng"
                      ? convertFeetString(s.value)
                      : convertFeet(s.value),
                }
              : s
          );
        }
      }
      return speed;
    },
  });
});

Hooks.once("babele.ready", () => {
  game.pf2e.ConditionManager.init();
});
